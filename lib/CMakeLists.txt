if(WIN32)
  set(additional_LINK_FLAGS -Wl,-subsystem,windows -Wl,-enable-auto-import -Wl,-enable-runtime-pseudo-reloc)
endif(WIN32)

add_definitions(-DDATA_PREFIX=\"${CMAKE_INSTALL_PREFIX}/share/${CMAKE_PROJECT_NAME}\")

file(GLOB_RECURSE LIB_SOURCES "*.cpp")
file(GLOB_RECURSE LIB_HEADERS "*.h" "*.hpp")

include(${QT_USE_FILE})

include_directories(
    ${CMAKE_CURRENT_BINARY_DIR} # for include headers generated from *.ui
    ${QT_INCLUDE_DIR}
)

add_definitions(
    ${QT_DEFINITIONS}
)

add_library(qavrhelpers STATIC ${LIB_SOURCES})

target_link_libraries(
    qavrhelpers
    ${QT_LIBRARIES}
    ${additional_LINK_FLAGS}
)

