#include <unistd.h>
#include <sys/select.h>

#include <cerrno>
#include <cstdio>
#include <cstring>
#include <list>
#include <vector>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <functional>
#include <iterator>

#include "avrdude.h"

using namespace std;

namespace {

const int BUFFER_SIZE = 512;

struct DummyOutputHandler : public unary_function<string, void>
{
    void operator() (const string&) {}
};

template<typename S = DummyOutputHandler, typename E = DummyOutputHandler>
class Executor {
public:
    int exec(const string& command,
             const vector<string>& args = list<string>(),
             S stdoutHandler = S(),
             E stderrHandler = E()) {
        int inpfd[2];
        int outfd[2];
        int errfd[2];

        pipe(inpfd);
        pipe(outfd);
        pipe(errfd);

        pid_t pid = fork();
        switch (pid) {
            case -1:
                return -1;
                break;

            case 0: // child
            {
                dup2(inpfd[0], STDIN_FILENO);
                dup2(outfd[1], STDOUT_FILENO);
                dup2(errfd[1], STDERR_FILENO);

                close(inpfd[1]);
                close(outfd[0]);
                close(errfd[0]);

                // exec here
                vector<char*> argv(args.size() + 2);
                argv[0]               = strndup(command.c_str(), command.size());
                argv[args.size() + 1] = 0;

                for (int i = 1; i <= args.size(); ++i) {
                    argv[i] = strndup(args[i - 1].c_str(), args[i - 1].size());
                }



                int r = execvp(command.c_str(), argv.data());

                cout << "result = " << r << ", errno = " << errno << ", strerror = " << strerror(errno) << endl;
                exit(-1);

                break;
            }

            default: // parent
                close(inpfd[0]);
                close(outfd[1]);
                close(errfd[1]);

                // TODO: do async
                int retval;
                fd_set rfds;

                char buf[BUFFER_SIZE];

                while (true) {

                    FD_ZERO(&rfds);
                    FD_SET(outfd[0], &rfds);
                    FD_SET(errfd[0], &rfds);

                    int maxfd = max(outfd[0], errfd[0]);

                    retval = select(maxfd + 1, &rfds, 0, 0, 0);

                    if (retval == -1)
                    {
                        cout << "Select fail" << endl;
                        return -1;
                    }
                    else
                    {
                        // DATA read
                        int stdoutReaded = 0;
                        int stdoutErrno  = 0;
                        int stderrReaded = 0;
                        int stderrErrno  = 0;


                        // read stdout
                        if (FD_ISSET(outfd[0], &rfds)) {
                            stdoutReaded = read(outfd[0], buf, BUFFER_SIZE - 1);

                            stdoutErrno = errno;

                            if (stdoutReaded > 0) {
                                buf[stdoutReaded] = 0;
                                stdoutHandler(buf);
                            }
                        }

                        // read stderr
                        if (FD_ISSET(errfd[0], &rfds)) {
                            stderrReaded = read(errfd[0], buf, BUFFER_SIZE - 1);

                            stderrErrno = errno;

                            if (stderrReaded > 0) {
                                buf[stderrReaded] = 0;
                                stderrHandler(buf);
                            }
                        }

                        // nodata
                        if (stdoutReaded == 0 && stderrReaded == 0) {
                            return 0;
                        } else if (stdoutReaded < 0) {
                            if (stdoutErrno != EINTR && stdoutErrno != EAGAIN) {
                                return -1;
                            }
                        } else if (stderrReaded < 0) {
                            if (stderrErrno != EINTR && stderrErrno != EAGAIN) {
                                return -1;
                            }
                        }
                    }
                }
                break;
        }
    }
};


list<string> splitString(const string& str, char delimiter = '\n')
{
    list<string> result;
    istringstream ist(str);
    string s;

    while (getline(ist, s, delimiter)) {
        result.push_back(s);
    }

    return result;
}

} // namespace anonymous

namespace avrdude {

struct UnitProcessor {
    enum State {
        INIT,
        READY,
        UNIT_ID,
        UNIT_DESC,
        UNIT_OTHER
    };

    UnitProcessor(list<ConfigUnit>& result)
        : result(result),
          state(INIT)
    {
    }

    void operator() (char ch)
    {
        switch (state)
        {
            case avrdude::UnitProcessor::UNIT_ID:
                if (ch == '=') {
                    state = UNIT_DESC;
                } else if (ch != ' ' && ch != '\t') {
                    unit.id += ch;
                }
                break;

            case avrdude::UnitProcessor::UNIT_DESC:
                if (ch == '[') {
                    state = UNIT_OTHER;
                } else if (!(ch == ' ' && (prev == ' ' || prev == '='))) {
                    unit.desc += ch;
                }
                break;

            case avrdude::UnitProcessor::UNIT_OTHER:
                if (ch == ']') {
                    state = READY;
                    result.push_back(unit);
                }
                break;

            case avrdude::UnitProcessor::INIT:
                if (ch == ':') {
                    state = READY;
                }
                break;

            case avrdude::UnitProcessor::READY:
                if (ch != '\n' && ch != ' ' && ch != '\t') {
                    state = UNIT_ID;

                    unit.id.clear();
                    unit.desc.clear();

                    unit.id += ch;
                }
                break;
        }

        prev = ch;
    }

    list<ConfigUnit>& result;
    ConfigUnit        unit;

    char  prev;
    State state;
};

struct OutputReader : public unary_function<string, void> {

    OutputReader(string &data) : data(data) {}

    void operator() (const string& data) {
        this->data.append(data);
    }

    string &data;
};


inline
bool operator<(const ConfigUnit& lhs, const ConfigUnit& rhs) {
    return (lhs.id < rhs.id);
}


list<ConfigUnit> getUnitList(const string &executableName, const string& arg, bool doDump = false)
{
    list<McuUnit> result;
    int ec;

    vector<string> args(2);
    args.push_back(arg);
    args.push_back("?");

    Executor<DummyOutputHandler, OutputReader> e;
    string raw;
    OutputReader reader(raw);

    ec = e.exec(executableName, args, DummyOutputHandler(), reader);

    if (ec == -1) {
        return result;
    }

    UnitProcessor processor(result);
    for_each(raw.begin(), raw.end(), processor);

    // sort data
    result.sort();

    if (doDump) {
        ostream_iterator<McuUnit> outit(cout, "\n");
        copy(result.begin(), result.end(), outit);
    }

    return result;
}


list<McuUnit> getMcuList(const string &executableName, bool doDump)
{
    return getUnitList(executableName, "-p", doDump);
}



list<ConfigUnit> getProgrammerList(const string &executableName, bool doDump)
{
    return getUnitList(executableName, "-c", doDump);
}

} // ::avrdude

