#ifndef AVRDUDE_H
#define AVRDUDE_H

#include <string>
#include <list>

namespace avrdude {

struct ConfigUnit {
    std::string id;
    std::string desc;
};

typedef ConfigUnit McuUnit;
typedef ConfigUnit ProgrammerUnit;


inline
std::ostream& operator<<(std::ostream &ost, const ConfigUnit& unit)
{
    ost << unit.id << ", " << unit.desc;
    return ost;
}


std::list<McuUnit>    getMcuList(const std::string& executableName = "avrdude", bool doDump = false);
std::list<ConfigUnit> getProgrammerList(const std::string& executableName = "avrdude", bool doDump = false);

}


#endif // AVRDUDE_H
